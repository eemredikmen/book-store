<?php

function CreateDb()
{

  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbName = "bookstore";

  //Create connetion
  $conn = mysqli_connect($servername, $username, $password);

  //Check connetion
  if (!$conn) {
    die("Connection Faild: " . mysqli_connect_error());
  }

  // Crate Database

  $sql = "CREATE DATABASE IF NOT EXISTS $dbName";

  if (mysqli_query($conn, $sql)) {

    $conn = mysqli_connect($servername, $username, $password, $dbName);

    $sql = "
    CREATE TABLE IF NOT EXISTS books(
        id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        book_name VARCHAR (25) NOT NULL,
        book_publisher VARCHAR (20),
        book_price FLOAT 
    );
";
    if (mysqli_query($conn, $sql)) {
      return $conn;
    } 
    else {
      echo 'Cannot created table';
    }
  } 
  else {
    echo 'Eror while creating database' . mysqli_error($conn);
  }
}
